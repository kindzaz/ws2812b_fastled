void encoderClickHandler() {
    currentState = nextState(currentState);
    Serial.println("State: " + String(currentState));
    initParameters();
}

int nextState(int currentState) {
  return (currentState + 1) % STATES_COUNT;
}

void initParameters() {
    switch (currentState) {
        case 0:
            break;
        case 1:
            encoderValue = 9;
            break;
        case 2:
            encoderValue = 9;
            break;
        case 3:
            bpm = 2;
            encoderValue = 3;
            break;
        case 4:
            break;
        case 5:
            break;
        case 6:
            break;
        case 7:
            encoderValue = 0;
            break;
    }
}

void encoderRotateHandler(){
    int MSB = digitalRead(PIN_ENCODER_A);
    int LSB = digitalRead(PIN_ENCODER_B);
    int encoded = (MSB << 1) | LSB;
    if (encoded == lastEncoded) {
        Serial.println("bounce");
        return;
    }

    if (MSB != LSB) encoderValue++;
    else encoderValue--;
    lastEncoded = encoded; //store this value for duplications (bounce) identification
    Serial.println("encoderValue: " + String(encoderValue));
}

int makeEncoderValuePositive() {
    if (encoderValue < 0) {
        encoderValue = 0;
    }

    return encoderValue;
}
