#include <FastLED.h>

#define PIN_BUTTON  7
#define PIN_ENCODER_A  3
#define PIN_ENCODER_B  2
#define PIN_LED     5

#define LEDS_COUNT    300
#define STATES_COUNT 8
#define LED_TYPE WS2811
#define MAX_BRIGHTNESS 255
#define PHASE 65534
#define FADE_VALUE 224

CRGB leds[LEDS_COUNT];
int currentState = 0;
int lastEncoded = 0;
int encoderValue = 2;
uint8_t bpm = 1; // beats per minute
uint32_t previousMillis = millis();
                                   
void setup() {
    Serial.begin(9600);
    pinMode(PIN_LED, OUTPUT);
    pinMode(PIN_ENCODER_A, INPUT_PULLUP);
    pinMode(PIN_ENCODER_B, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(PIN_ENCODER_A), encoderRotateHandler, CHANGE);   // 0
    attachInterrupt(digitalPinToInterrupt(PIN_BUTTON), encoderClickHandler, FALLING); // 4
  
    LEDS.addLeds<LED_TYPE, PIN_LED, GRB>(leds, LEDS_COUNT);
    FastLED.setBrightness(MAX_BRIGHTNESS);
    set_max_power_in_volts_and_milliamps(5, 1000);
    FastLED.clear();
}
  
void loop() {
    switch (currentState) {
        case 0:
            dotGreenRed();
            break;
        case 1:
            dotRgbQueue();
            break;
        case 2:
            dotColorsQueue();
            break;
        case 3:
            dotRgbFollowAround();
            break;
        case 4:
            rainbow();
            break;
        case 5:
            sawtoothWave();
            break;
        case 6:
            randomPixel();
            break;
        case 7:
            rangePlay();
            break;
    }

    FastLED.show();
}

void dotGreenRed() {
    bpm = makeEncoderValuePositive();
    uint16_t red = beatsin16(bpm, 0, LEDS_COUNT-1, 0, PHASE/2);
    uint16_t green = beatsin16(bpm, 0, LEDS_COUNT-1);
    leds[red] = CRGB::Red;
    leds[green] = CRGB::Green;
    nscale8(leds, LEDS_COUNT, FADE_VALUE);  
}

void dotRgbQueue() {
    uint32_t timeOffset = encoderValue * 100;
    leds[beatsin16(bpm, 0, LEDS_COUNT-1, 0)] = CRGB::Red;
    leds[beatsin16(bpm, 0, LEDS_COUNT-1, timeOffset)] = CRGB::Green;
    leds[beatsin16(bpm, 0, LEDS_COUNT-1, timeOffset * 2)] = CRGB::Blue;
    nscale8(leds, LEDS_COUNT, FADE_VALUE);  
}

void dotColorsQueue() {
    uint32_t timeOffset = encoderValue * 100;
    leds[beatsin16(bpm, 0, LEDS_COUNT-1, 0)] = CRGB::Red;
    leds[beatsin16(bpm, 0, LEDS_COUNT-1, timeOffset)] = CRGB::Green;
    leds[beatsin16(bpm, 0, LEDS_COUNT-1, timeOffset * 2)] = CRGB::Blue;
    leds[beatsin16(bpm, 0, LEDS_COUNT-1, timeOffset * 3)] = CRGB::Chocolate;
    leds[beatsin16(bpm, 0, LEDS_COUNT-1, timeOffset * 4)] = CRGB::Fuchsia;
    leds[beatsin16(bpm, 0, LEDS_COUNT-1, timeOffset * 5)] = CRGB::Yellow;
    leds[beatsin16(bpm, 0, LEDS_COUNT-1, timeOffset * 6)] = CRGB::White;
    nscale8(leds, LEDS_COUNT, FADE_VALUE);  
}

void dotRgbFollowAround() {
    int toValue = encoderValue * 1000;
    uint32_t timeOffsetRed = beatsin16(30, 0, toValue); //beats per minute, from, to
    uint32_t timeOffsetGreen = beatsin16(15, 0, toValue);
    uint32_t timeOffsetBlue = beatsin16(20, 0, toValue);
    
    uint16_t redIndex = beatsin16(bpm, 0, LEDS_COUNT-1, 0 + timeOffsetRed);
    uint16_t greenIndex = beatsin16(bpm, 0, LEDS_COUNT-1, 500 + timeOffsetGreen);
    uint16_t blueIndex = beatsin16(bpm, 0, LEDS_COUNT-1, 1000 + timeOffsetBlue);
    
    leds[redIndex] = CRGB::Red;
    leds[greenIndex] = CRGB::Green;
    leds[blueIndex] = CRGB::Blue;
    nscale8(leds, LEDS_COUNT, FADE_VALUE);  
}

void rainbow() {
    uint8_t beatA = beatsin8(17, 0, 255);                        // Starting hue
    fill_rainbow(leds, LEDS_COUNT, beatA, encoderValue);            // Use FastLED's fill_rainbow routine.
}

void sawtoothWave() {
    bpm = makeEncoderValuePositive();
    uint16_t led1 = map(beat8(bpm), 0, 255, 0, LEDS_COUNT-1);
//  Serial.println("led1: " + String(led1));
    leds[led1] = CRGB::Green;
    nscale8(leds, LEDS_COUNT, FADE_VALUE);
}

void randomPixel() {
    bpm = makeEncoderValuePositive();
    if (millis() - previousMillis > bpm * 100) {
        previousMillis = millis();
        leds[random16(LEDS_COUNT)] = CRGB(random8(), 0, 0);
        leds[random16(LEDS_COUNT)] = CRGB(0, random8(), 0);
        leds[random16(LEDS_COUNT)] = CRGB(0, 0, random8());
        leds[random16(LEDS_COUNT)] = CRGB(random8(), random8(), 0);  
        leds[random16(LEDS_COUNT)] = CRGB(0, random8(), random8());  
        leds[random16(LEDS_COUNT)] = CRGB(random8(), 0, random8());
    }
    
    nscale8(leds, LEDS_COUNT, FADE_VALUE);                                             
}

void rangePlay() {
    fill_gradient (leds, LEDS_COUNT/2, CHSV(80, 255, 255), LEDS_COUNT/2 + encoderValue * 3, CHSV(0, 255, 255), SHORTEST_HUES);
    nscale8(leds, LEDS_COUNT, FADE_VALUE); 
}
